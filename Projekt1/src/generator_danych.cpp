#include "../include/generator_danych.h"
#include <random>

using namespace std;

int* Generator_danych::generuj_zestaw_testowy(int rozmiar) {
  int* zestaw = new int [rozmiar];
  random_device rd;
  mt19937 gen(rd());
  uniform_int_distribution<> dist(1, 1000000);
  for(int i=0; i<rozmiar; i++) {
    *(zestaw+i)=dist(gen);
  }
  return zestaw;
}

int Generator_danych::generuj_liczbe(int rozmiar) {
  random_device rd;
  mt19937 gen(rd());
  uniform_int_distribution<> dist(0, rozmiar);
  return dist(gen);
}
