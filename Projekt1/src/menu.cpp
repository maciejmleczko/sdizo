#include "../include/menu.h"
#include "../include/kopiec.h"
#include <fstream>
#include <iomanip>
#include <string>

Menu::Menu() {

}

Menu::~Menu() {

}

void Menu::uruchom_program() {
  bool aktywne_menu=true;
  while(aktywne_menu) {
    system("clear");
    wyswietl_menu_glowne();
    wybor=getchar();
    switch(wybor) {
      case '1':
        wyswietl_menu_testow();
      break;
      case '2':
        wyswietl_dostepne_struktury();
      break;
      case '3':
        wyswietl_podmenu_wczytywania();
      break;
      case '4':
        aktywne_menu=false;
        system("clear");
      break;
      default:
        cout<<"Zła opcja"<<endl;
      break;
    }
  }
}

void Menu::wyswietl_menu_testow() {
  bool aktywne_menu = true;
  while(aktywne_menu) {
    system("clear");
    cout<<endl<<endl<<endl;
    cout<<"1.  Lista    | dodawanie n liczb na dowolną pozycje"<<endl;
    cout<<"2.  Lista    | dodawanie na początek"<<endl;
    cout<<"3.  Lista    | dodawanie na koniec"<<endl;
    cout<<"4.  Lista    | usuwanie z dowolnej pozycji"<<endl;
    cout<<"5.  Lista    | usuwanie z początku"<<endl;
    cout<<"6.  Lista    | usuwanie z końca"<<endl;
    cout<<"7.  Lista    | wyszukiwanie"<<endl;

    cout<<"8.  Tablica  | dodawanie"<<endl;
    cout<<"9.  Tablica  | usuwanie"<<endl;
    cout<<"A.  Tablica  | wyszukiwanie"<<endl;

    cout<<"B.  Kopiec   | dodawanie"<<endl;
    cout<<"C.  Kopiec   | usuwanie"<<endl;
    cout<<"D.  Kopiec   | wyszukiwanie"<<endl;
    cout<<"E.  Powrót"<<endl;
    wybor=getchar();
    double czas=0;
    int rozmiar_zestawu_tmp = test.rozmiar_zestawu;
    switch(wybor) {


      case '1':
      for(int r=20;r>0;r--) {
      for(int i=0; i<100; i++) {
        test.generuj_nowy_zestaw();
        test.wyczysc_aktywna_strukture();
        test.generuj_lista();
        czas+=test.dodawanie_do_srodka_zbioru_lista();
      }
      czas/=100.0;
      cout<<fixed;
      cout<<setprecision(9);
      cout<<test.rozmiar_zestawu<<";"<<czas<<endl;
      test.rozmiar_zestawu+=rozmiar_zestawu_tmp;
    }
    test.rozmiar_zestawu=rozmiar_zestawu_tmp;
    getchar();
    getchar();
        break;


      case '2':
      for(int r=20;r>0;r--) {
      for(int i=0; i<100; i++) {
        test.generuj_nowy_zestaw();
        test.wyczysc_aktywna_strukture();
        test.generuj_lista();
        czas+=test.dodawanie_na_poczatek_lista();
      }
      czas/=100.0;
      cout<<fixed;
      cout<<setprecision(9);
      cout<<test.rozmiar_zestawu<<";"<<czas<<endl;
      test.rozmiar_zestawu+=rozmiar_zestawu_tmp;
    }
    test.rozmiar_zestawu=rozmiar_zestawu_tmp;
    getchar();
    getchar();
        break;


      case '3':
      for(int r=20;r>0;r--) {
      for(int i=0; i<100; i++) {
        test.generuj_nowy_zestaw();
        test.wyczysc_aktywna_strukture();
        test.generuj_lista();
        czas+=test.dodawanie_na_koniec_lista();
      }
      czas/=100.0;
      cout<<fixed;
      cout<<setprecision(9);
      cout<<test.rozmiar_zestawu<<";"<<czas<<endl;
      test.rozmiar_zestawu+=rozmiar_zestawu_tmp;
    }
    test.rozmiar_zestawu=rozmiar_zestawu_tmp;
    getchar();
    getchar();
        break;


      case '4':
      for(int r=20;r>0;r--) {
      for(int i=0; i<100; i++) {
        test.generuj_nowy_zestaw();
        test.wyczysc_aktywna_strukture();
        test.generuj_lista();
        czas+=test.usuwanie_ze_srodka_zbioru_lista();
      }
      czas/=100.0;
      cout<<fixed;
      cout<<setprecision(9);
      cout<<test.rozmiar_zestawu<<";"<<czas<<endl;
      test.rozmiar_zestawu+=rozmiar_zestawu_tmp;
    }
    test.rozmiar_zestawu=rozmiar_zestawu_tmp;
    getchar();
    getchar();
        break;


      case '5':
      for(int r=20;r>0;r--) {
      for(int i=0; i<100; i++) {
        test.generuj_nowy_zestaw();
        test.wyczysc_aktywna_strukture();
        test.generuj_lista();
        czas+=test.usuwanie_z_poczatku();
      }
      czas/=100.0;
      cout<<fixed;
      cout<<setprecision(9);
      cout<<test.rozmiar_zestawu<<";"<<czas<<endl;
      test.rozmiar_zestawu+=rozmiar_zestawu_tmp;
    }
    test.rozmiar_zestawu=rozmiar_zestawu_tmp;
    getchar();
    getchar();
        break;


      case '6':
      for(int r=20;r>0;r--) {
      for(int i=0; i<100; i++) {
        test.generuj_nowy_zestaw();
        test.wyczysc_aktywna_strukture();
        test.generuj_lista();
        czas+=test.usuwanie_z_konca();
      }
      czas/=100.0;
      cout<<fixed;
      cout<<setprecision(9);
      cout<<test.rozmiar_zestawu<<";"<<czas<<endl;
      test.rozmiar_zestawu+=rozmiar_zestawu_tmp;
    }
    test.rozmiar_zestawu=rozmiar_zestawu_tmp;
    getchar();
    getchar();
        break;


      case '7':
      for(int r=20;r>0;r--) {
      for(int i=0; i<100; i++) {
        test.generuj_nowy_zestaw();
        test.wyczysc_aktywna_strukture();
        test.generuj_lista();
        czas+=test.wyszukiwanie_lista();
      }
      czas/=100.0;
      cout<<fixed;
      cout<<setprecision(9);
      cout<<test.rozmiar_zestawu<<";"<<czas<<endl;
      test.rozmiar_zestawu+=rozmiar_zestawu_tmp;
    }
    test.rozmiar_zestawu=rozmiar_zestawu_tmp;
    getchar();
    getchar();
        break;


      case '8':
      for(int r=20;r>0;r--) {
      for(int i=0; i<100; i++) {
        test.generuj_nowy_zestaw();
        test.wyczysc_aktywna_strukture();
        test.generuj_tablica();
        czas+=test.dodawanie_tablica();
      }
      czas/=100.0;
      cout<<fixed;
      cout<<setprecision(9);
      cout<<test.rozmiar_zestawu<<";"<<czas<<endl;
      test.rozmiar_zestawu+=rozmiar_zestawu_tmp;
    }
    test.rozmiar_zestawu=rozmiar_zestawu_tmp;
    getchar();
    getchar();
        break;


      case '9':
      for(int r=20;r>0;r--) {
      for(int i=0; i<100; i++) {
        test.generuj_nowy_zestaw();
        test.wyczysc_aktywna_strukture();
        test.generuj_tablica();
        czas+=test.usuwanie_tablica();
      }
      czas/=100.0;
      cout<<fixed;
      cout<<setprecision(9);
      cout<<test.rozmiar_zestawu<<";"<<czas<<endl;
      test.rozmiar_zestawu+=rozmiar_zestawu_tmp;
    }
    test.rozmiar_zestawu=rozmiar_zestawu_tmp;
    getchar();
    getchar();
        break;


      case 'A':
      for(int r=20;r>0;r--) {
      for(int i=0; i<100; i++) {
        test.generuj_nowy_zestaw();
        test.wyczysc_aktywna_strukture();
        test.generuj_tablica();
        czas+=test.wyszukiwanie_tablica();
      }
      czas/=100.0;
      cout<<fixed;
      cout<<setprecision(9);
      cout<<test.rozmiar_zestawu<<";"<<czas<<endl;
      test.rozmiar_zestawu+=rozmiar_zestawu_tmp;
    }
    test.rozmiar_zestawu=rozmiar_zestawu_tmp;
    getchar();
    getchar();
        break;


      case 'B':
      for(int r=20;r>0;r--) {
      for(int i=0; i<100; i++) {
        test.generuj_nowy_zestaw();
        test.wyczysc_aktywna_strukture();
        test.generuj_kopiec();
        czas+=test.dodawanie_kopiec();
      }
      czas/=100.0;
      cout<<fixed;
      cout<<setprecision(9);
      cout<<test.rozmiar_zestawu<<";"<<czas<<endl;
      test.rozmiar_zestawu+=rozmiar_zestawu_tmp;
    }
    test.rozmiar_zestawu=rozmiar_zestawu_tmp;
    getchar();
    getchar();
        break;


      case 'C':
      for(int r=20;r>0;r--) {
      for(int i=0; i<100; i++) {
        test.generuj_nowy_zestaw();
        test.wyczysc_aktywna_strukture();
        test.generuj_kopiec();
        czas+=test.usuwanie_wierzcholka_kopca();
      }
      czas/=100.0;
      cout<<fixed;
      cout<<setprecision(9);
      cout<<test.rozmiar_zestawu<<";"<<czas<<endl;
      test.rozmiar_zestawu+=rozmiar_zestawu_tmp;
    }
    test.rozmiar_zestawu=rozmiar_zestawu_tmp;
    getchar();
    getchar();
        break;


      case 'D':
      for(int r=20;r>0;r--) {
      for(int i=0; i<100; i++) {
        test.generuj_nowy_zestaw();
        test.wyczysc_aktywna_strukture();
        test.generuj_kopiec();
        czas+=test.wyszukiwanie_kopiec();
      }
      czas/=100.0;
      cout<<fixed;
      cout<<setprecision(9);
      cout<<test.rozmiar_zestawu<<";"<<czas<<endl;
      test.rozmiar_zestawu+=rozmiar_zestawu_tmp;
    }
    test.rozmiar_zestawu=rozmiar_zestawu_tmp;
    getchar();
    getchar();
        break;


      case 'E':
        aktywne_menu=false;
        break;


      default:
        cout<<"Zła opcja"<<endl;
      break;
    }
  }
}

void Menu::wczytaj_dane_testowe() {
  fstream plik;
  int rozmiar;
  int bufor;
  int* tab;
  string nazwa;
  cout<<endl<<endl;
  cout<<"Podaj nazwe pliku z rozszerzeniem: ";
  cin>>nazwa;
  plik.open(nazwa);
  if(plik.is_open())
  {
      plik >> rozmiar;
    if(plik.fail())
      cout << "plik bład - READ rozmiar" << endl;
    else
      tab = new int[rozmiar];
      test.rozmiar_zestawu=rozmiar;
      for(int i = 0; i < rozmiar; i++)
      {
        plik >> bufor;
        if(plik.fail())
        {
          cout << "plik bład - READ DATA" << endl;
          break;
        }
        else
          tab[i] = bufor;
      }
      test.zestaw=tab;
      plik.close();
  }
else
    cout << "plik błąd - OPEN" << endl;
plik.close();
}

void Menu::wyswietl_menu_glowne() {
  cout<<endl<<endl<<endl;
  cout<<"1. Wybór testu"<<endl;
  cout<<"2. Wybór struktury"<<endl;
  cout<<"3. Wczytaj dane"<<endl;
  cout<<"4. Wyjdź"<<endl;
}

void Menu::wyswietl_dostepne_struktury() {
  bool aktywne_menu = true;
  while(aktywne_menu) {
    system("clear");
    cout<<endl<<endl<<endl;
    cout<<"1. Tablica"<<endl;
    cout<<"2. Lista dwukierunkowa"<<endl;
    cout<<"3. Kopiec"<<endl;
    cout<<"4. Wyczyść aktywne struktury"<<endl;
    cout<<"5. Wyświetl obecne struktury"<<endl<<endl;
    cout<<"6. Dodaj do tablicy"<<endl;
    cout<<"7. Dodaj do listy"<<endl;
    cout<<"8. Dodaj do kopca"<<endl;
    cout<<"9. Usuń z tablicy"<<endl;
    cout<<"A. Usuń z listy"<<endl;
    cout<<"B. Usuń z kopca"<<endl;
    cout<<"C. Wyszukaj w tablicy"<<endl;
    cout<<"D. Wyszukaj w liście"<<endl;
    cout<<"E. Wyszukaj w kopcu"<<endl;
    cout<<"F. Powrót"<<endl;
    int pozycja;
    int wartosc;
    wybor=getchar();
    switch(wybor) {
      case '1':
        test.generuj_tablica();
        break;
      case '2':
        test.generuj_lista();
        break;
      case '3':
        test.generuj_kopiec();
        break;
      case '4':
        test.wyczysc_aktywna_strukture();
        break;
      case '5':
        if(test.lista!=0) {
          cout<<endl<<endl;
          cout<<"Lista:";
          test.lista->wypisz();
        }
        if(test.tablica!=0) {
          cout<<endl<<endl;
          cout<<"Tablica:";
          test.tablica->wypisz();
        }
        if(test.kopiec!=0) {
          cout<<endl<<endl;
          cout<<"Kopiec:";
          test.kopiec->wypisz();
        }
        getchar();
        getchar();
        break;
      case '6':
        cout<<endl<<endl<<endl;
        cout<<"Podaj pozycje: ";
        cin>>pozycja;
        cout<<"Podaj wartosc: ";
        cin>>wartosc;
        test.tablica->dodaj(pozycja,wartosc);
        test.tablica->wypisz();
        getchar();
        getchar();
        break;

        case '7':
          cout<<endl<<endl<<endl;
          cout<<"Podaj pozycje: ";
          cin>>pozycja;
          cout<<"Podaj wartosc: ";
          cin>>wartosc;
          test.lista->dodaj(pozycja,wartosc);
          test.lista->wypisz();
          getchar();
          getchar();
          break;

        case '8':
          cout<<endl<<endl<<endl;
          cout<<"Podaj wartosc: ";
          cin>>wartosc;
          test.kopiec->dodaj_ost(wartosc);
          test.kopiec->wypisz();
          getchar();
          getchar();
          break;

          case '9':
            cout<<endl<<endl<<endl;
            cout<<"Podaj pozycje: ";
            cin>>pozycja;
            test.tablica->usun(pozycja);
            test.tablica->wypisz();
            getchar();
            getchar();
            break;


            case 'A':
              cout<<endl<<endl<<endl;
              cout<<"Podaj pozycje: ";
              cin>>pozycja;
              test.lista->usun(pozycja);
              test.lista->wypisz();
              getchar();
              getchar();
              break;

              case 'B':
                cout<<endl<<endl<<endl;
                test.kopiec->usun_ze_szczytu();
                test.kopiec->wypisz();
                getchar();
                getchar();
                break;

                case 'C':
                  cout<<endl<<endl<<endl;
                  cout<<"Podaj wartosc: ";
                  cin>>wartosc;
                  cout<<test.tablica->wyszukaj(wartosc);
                  getchar();
                  getchar();
                  break;


                  case 'D':
                    cout<<endl<<endl<<endl;
                    cout<<"Podaj wartosc: ";
                    cin>>wartosc;
                    cout<<test.lista->wyszukaj(wartosc);
                    getchar();
                    getchar();
                    break;


                    case 'E':
                      cout<<endl<<endl<<endl;
                      cout<<"Podaj wartosc: ";
                      cin>>wartosc;
                      cout<<test.kopiec->wyszukaj(wartosc);
                      getchar();
                      getchar();
                      break;
      case 'F':
        aktywne_menu=false;
      break;
      default:
        cout<<"Zła opcja"<<endl;
      break;
    }
  }
}

void Menu::wyswietl_podmenu_wczytywania() {
  bool aktywne_menu = true;
  while(aktywne_menu) {
    system("clear");
    cout<<endl<<endl<<endl;
    cout<<"1. Wczytaj dane z pliku"<<endl;
    cout<<"2. Dane losowe"<<endl;
    cout<<"3. Powrót"<<endl;
    wybor=getchar();
    switch(wybor) {
      case '1':
          wczytaj_dane_testowe();
        break;
      case '2':
        cout<<endl<<endl;
        cout<<"Podaj ilość danych: ";
        cin>>test.rozmiar_zestawu;
        test.generuj_nowy_zestaw();
        break;
      case '3':
        aktywne_menu=false;
        break;
      default:
        cout<<"Zła opcja"<<endl;
      break;
    }
  }
}

void Menu::uruchom_test() {

}
