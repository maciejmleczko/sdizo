#include "../include/kopiec.h"
#include "math.h"
#include <iostream>

using namespace std;

Kopiec::Kopiec() {
  kopiec = 0;
  liczba_elementow_kopca=0;
}

Kopiec::~Kopiec() {
  delete[] kopiec;
  kopiec=0;
}

void Kopiec::dodaj_ost(int wartosc_do_dodania) {
  int* wiekszy_kopiec;
  liczba_elementow_kopca++;
  wiekszy_kopiec= new int[liczba_elementow_kopca];
  for(int i=0; i<(liczba_elementow_kopca-1); i++){
    *(wiekszy_kopiec+i)=*(kopiec+i);
  }
  *(wiekszy_kopiec+(liczba_elementow_kopca-1))=wartosc_do_dodania;
  delete[] kopiec;
  kopiec=wiekszy_kopiec;
  napraw_w_gore();
}

void Kopiec::usun_ze_szczytu() {
  int* mniejszy_kopiec;
  liczba_elementow_kopca--;
  mniejszy_kopiec= new int[liczba_elementow_kopca];
  for(int i=1; i<liczba_elementow_kopca; i++) {
    *(mniejszy_kopiec+i)=*(kopiec+i);
  }
  *(mniejszy_kopiec)=*(kopiec+(liczba_elementow_kopca));
  delete[] kopiec;
  kopiec = mniejszy_kopiec;
  napraw_w_dol();
}

void Kopiec::napraw_w_gore() {
  int syn = liczba_elementow_kopca-1;
  int rodzic =floor((syn-1)/2);
  while(*(kopiec+rodzic)<*(kopiec+syn)&&syn>0) {
    swap(*(kopiec+rodzic),*(kopiec+syn));
    syn=rodzic;
    rodzic = floor((syn-1)/2);
  }
}

void Kopiec::napraw_w_dol() {
  int rodzic=0;
  int lewy_syn=2*rodzic+1;
  int prawy_syn=2*rodzic+2;
  int maksymalny_syn;
  while(lewy_syn<liczba_elementow_kopca) {
    if(prawy_syn>=liczba_elementow_kopca) {
      maksymalny_syn=lewy_syn;
    }
    else if(*(kopiec+lewy_syn)<*(kopiec+prawy_syn)) {
      maksymalny_syn=prawy_syn;
    }
    else maksymalny_syn=lewy_syn;
    if(*(kopiec+rodzic)>*(kopiec+maksymalny_syn)) {
      return;
    }
    swap(*(kopiec+rodzic),*(kopiec+maksymalny_syn));
    rodzic=maksymalny_syn;
    lewy_syn=2*rodzic+1;
    prawy_syn=2*rodzic+2;
  }
}

int* Kopiec::wyszukaj(int wartosc_do_wyszukania) {
  if(wartosc_do_wyszukania>*kopiec) return 0;
  for(int i=0; i<liczba_elementow_kopca; i++) {
    if(*(kopiec+i)==wartosc_do_wyszukania) {
      return (kopiec+i);
    }
  }
  return 0;
}

void Kopiec::wypisz() {
  int potega=1;
  for(int i=0; i<liczba_elementow_kopca; i++) {
    cout<<*(kopiec+i)<<' ';
    if(pow(2,potega)-2==i) {
      cout<<"| ";
      potega++;
    }
  }
  cout<<endl;
}
