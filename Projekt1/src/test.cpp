#include "../include/test.h"
#include "../include/generator_danych.h"
#include <time.h>
#include <iostream>
#include <iomanip>
#include <chrono>

using namespace std;

Test::Test() {
  zestaw=0;
  rozmiar_zestawu=0;
  lista = 0;
  tablica = 0;
  kopiec = 0;
}

Test::~Test() {

}

void Test::generuj_nowy_zestaw() {
  Generator_danych gen;
  if(zestaw!=0) {
    delete[] zestaw;
  }
  zestaw = gen.generuj_zestaw_testowy(rozmiar_zestawu);
}

void Test::generuj_lista() {
  lista = new Lista();
  for(int i=0; i<rozmiar_zestawu; i++) {
    lista->dodaj_na_koniec(*(zestaw+i));
  }
}

void Test::generuj_tablica() {
  tablica = new Tablica();
  for(int i=0; i<rozmiar_zestawu; i++) {
    tablica->dodaj(i,*(zestaw+i));
  }
}

void Test::generuj_kopiec() {
  kopiec = new Kopiec();
  for(int i=0; i<rozmiar_zestawu; i++) {
    kopiec->dodaj_ost(*(zestaw+i));
  }
}

void Test::wyczysc_aktywna_strukture() {
    if(lista!=0) {
      delete lista;
      lista=0;
    }
    if(tablica!=0) {
      delete tablica;
      tablica=0;
    }
    if(kopiec!=0) {
      delete kopiec;
      kopiec=0;
    }
}

double Test::dodawanie_tablica() {
  Generator_danych gen;
  int pozycja=gen.generuj_liczbe(tablica->liczba_elementow_tablicy-1);
  int wartosc=gen.generuj_liczbe(100000);
  auto start = std::chrono::high_resolution_clock::now();
  tablica->dodaj(pozycja,wartosc);
  auto koniec = std::chrono::high_resolution_clock::now();
  return std::chrono::duration_cast<std::chrono::duration<double> >(koniec - start).count();
}

double Test::dodawanie_do_srodka_zbioru_lista() {
  Generator_danych gen;
  int pozycja=gen.generuj_liczbe(lista->liczba_elementow_listy-1);
  int wartosc=gen.generuj_liczbe(100000);
  auto start = std::chrono::high_resolution_clock::now();
  lista->dodaj(pozycja,wartosc);
  auto koniec = std::chrono::high_resolution_clock::now();
  return std::chrono::duration_cast<std::chrono::duration<double> >(koniec - start).count();
}

double Test::dodawanie_na_poczatek_lista() {
  Generator_danych gen;
  int wartosc=gen.generuj_liczbe(100000);
  auto start = std::chrono::high_resolution_clock::now();
  lista->dodaj_na_poczatek(wartosc);
  auto koniec = std::chrono::high_resolution_clock::now();
  return std::chrono::duration_cast<std::chrono::duration<double> >(koniec - start).count();
}

double Test::dodawanie_na_koniec_lista() {
  Generator_danych gen;
  int wartosc=gen.generuj_liczbe(100000);
  auto start = std::chrono::high_resolution_clock::now();
  lista->dodaj_na_koniec(wartosc);
  auto koniec = std::chrono::high_resolution_clock::now();
  return std::chrono::duration_cast<std::chrono::duration<double> >(koniec - start).count();
}

double Test::dodawanie_kopiec() {
  Generator_danych gen;
  int wartosc=gen.generuj_liczbe(100000);
  auto start = std::chrono::high_resolution_clock::now();
  kopiec->dodaj_ost(wartosc);
  auto koniec = std::chrono::high_resolution_clock::now();
  return std::chrono::duration_cast<std::chrono::duration<double> >(koniec - start).count();
}

double Test::usuwanie_tablica() {
  Generator_danych gen;
  int pozycja=gen.generuj_liczbe(tablica->liczba_elementow_tablicy-1);
  auto start = std::chrono::high_resolution_clock::now();
  tablica->usun(pozycja);
  auto koniec = std::chrono::high_resolution_clock::now();
  return std::chrono::duration_cast<std::chrono::duration<double> >(koniec - start).count();
}
double Test::usuwanie_ze_srodka_zbioru_lista() {
  Generator_danych gen;
  int pozycja=gen.generuj_liczbe(lista->liczba_elementow_listy-1);
  auto start = std::chrono::high_resolution_clock::now();
  lista->usun(pozycja);
  auto koniec = std::chrono::high_resolution_clock::now();
  return std::chrono::duration_cast<std::chrono::duration<double> >(koniec - start).count();
}

double Test::usuwanie_z_poczatku() {
  auto start = std::chrono::high_resolution_clock::now();
  lista->usun_z_poczatku();
  auto koniec = std::chrono::high_resolution_clock::now();
  return std::chrono::duration_cast<std::chrono::duration<double> >(koniec - start).count();
}

double Test::usuwanie_z_konca() {
  auto start = std::chrono::high_resolution_clock::now();
  lista->usun_z_konca();
  auto koniec = std::chrono::high_resolution_clock::now();
  return std::chrono::duration_cast<std::chrono::duration<double> >(koniec - start).count();
}

double Test::usuwanie_wierzcholka_kopca() {
  auto start = std::chrono::high_resolution_clock::now();
  kopiec->usun_ze_szczytu();
  auto koniec = std::chrono::high_resolution_clock::now();
  return std::chrono::duration_cast<std::chrono::duration<double> >(koniec - start).count();
}

double Test::wyszukiwanie_tablica() {
  Generator_danych gen;
  int wartosc=gen.generuj_liczbe(100000);
  auto start = std::chrono::high_resolution_clock::now();
  tablica->wyszukaj(wartosc);
  auto koniec = std::chrono::high_resolution_clock::now();
  return std::chrono::duration_cast<std::chrono::duration<double> >(koniec - start).count();
}

double Test::wyszukiwanie_lista() {
  Generator_danych gen;
  int wartosc=gen.generuj_liczbe(100000);
  auto start = std::chrono::high_resolution_clock::now();
  lista->wyszukaj(wartosc);
  auto koniec = std::chrono::high_resolution_clock::now();
  return std::chrono::duration_cast<std::chrono::duration<double> >(koniec - start).count();
}

double Test::wyszukiwanie_kopiec() {
  Generator_danych gen;
  int wartosc=gen.generuj_liczbe(100000);
  auto start = std::chrono::high_resolution_clock::now();
  kopiec->wyszukaj(wartosc);
  auto koniec = std::chrono::high_resolution_clock::now();
  return std::chrono::duration_cast<std::chrono::duration<double> >(koniec - start).count();
}
