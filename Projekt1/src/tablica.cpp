#include "../include/tablica.h"
#include <iostream>

using namespace std;

Tablica::Tablica() {
  tablica=0;
  liczba_elementow_tablicy=0;
}

Tablica::~Tablica() {
  delete[] tablica;
  tablica = 0;
}

void Tablica::dodaj(int pozycja_do_dodania, int wartosc_do_dodania) {
  if(pozycja_do_dodania>=0 && pozycja_do_dodania<=liczba_elementow_tablicy) {
    liczba_elementow_tablicy++;
    int* tablica_poszerzona = new int[liczba_elementow_tablicy];
    for(int i=0, j=0; j<liczba_elementow_tablicy; i++, j++) {
      if(j==pozycja_do_dodania) {
        *(tablica_poszerzona+j)=wartosc_do_dodania;
        i--;
      }
      else *(tablica_poszerzona+j)=*(tablica+i);
  }
  delete[] tablica;
  tablica=tablica_poszerzona;
 }
}

void Tablica::usun(int pozycja_do_usuniecia) {
  if(pozycja_do_usuniecia>=0 && pozycja_do_usuniecia<=liczba_elementow_tablicy) {
    liczba_elementow_tablicy--;
    int* tablica_pomniejszona=new int[liczba_elementow_tablicy];
    for(int i=0, j=0; i<liczba_elementow_tablicy; i++,j++) {
      if(j==pozycja_do_usuniecia) {
        i++;
        *(tablica_pomniejszona+j)=*(tablica+i);
      }
      else *(tablica_pomniejszona+j)=*(tablica+i);
    }
    delete[] tablica;
    tablica=tablica_pomniejszona;
  }
}

int* Tablica::wyszukaj(int wartosc_do_wyszukania) {
  for(int i=0; i<liczba_elementow_tablicy; i++) {
    if(*(tablica+i)==wartosc_do_wyszukania) {
      return (tablica+i);
    }
  }
  return 0;
}

void Tablica::wypisz() {
  for(int i=0; i<liczba_elementow_tablicy; i++) {
    cout<<*(tablica+i)<<"  ";
  }
}
