#include "../include/lista.h"
#include <iostream>

using namespace std;

Lista::Lista() {
  glowa=0;
  ogon=0;
  liczba_elementow_listy=0;
}

Lista::~Lista() {
  for(int i=0; i<liczba_elementow_listy; i++){
    usun_z_konca();
  }
}

void Lista::dodaj(int pozycja_do_dodania, int wartosc_do_dodania) {
  if(pozycja_do_dodania>=0 && pozycja_do_dodania<liczba_elementow_listy) {
    if(pozycja_do_dodania==0) {
        dodaj_na_poczatek(wartosc_do_dodania);
    }
    else if (pozycja_do_dodania==liczba_elementow_listy-1) {
      dodaj_na_koniec(wartosc_do_dodania);
    }
    else {
      if(liczba_elementow_listy!=0) {
        Element_listy* wsk_na_element;
        int iterator_pozycji;
        //dodawanie wzgledem odległości od środka zbioru
          if(pozycja_do_dodania<(liczba_elementow_listy/2)) {
            wsk_na_element=glowa;
            iterator_pozycji=0;
            while(iterator_pozycji!=pozycja_do_dodania) {
              wsk_na_element=wsk_na_element->nastepny;
              iterator_pozycji++;
            }
          }
          else {
            wsk_na_element=ogon;
            iterator_pozycji=liczba_elementow_listy-1;
            while(iterator_pozycji!=pozycja_do_dodania) {
              wsk_na_element=wsk_na_element->poprzedni;
              iterator_pozycji--;
            }
          }
          Element_listy* nowy_element = new Element_listy;
          nowy_element->war=wartosc_do_dodania;
          nowy_element->nastepny=wsk_na_element;
          nowy_element->poprzedni=wsk_na_element->poprzedni;
          wsk_na_element->poprzedni->nastepny=nowy_element;
          liczba_elementow_listy++;
        }
      }
  }
    else if (pozycja_do_dodania==0 && pozycja_do_dodania<=liczba_elementow_listy)
      dodaj_do_pustego(wartosc_do_dodania);
}

void Lista::dodaj_do_pustego(int wartosc_do_dodania) {
  Element_listy* nowy_element = new Element_listy;
  nowy_element->war=wartosc_do_dodania;
  nowy_element->poprzedni=0;
  nowy_element->nastepny=0;
  glowa=ogon=nowy_element;
  liczba_elementow_listy++;
}

void Lista::dodaj_na_poczatek(int wartosc_do_dodania) {
  if(liczba_elementow_listy!=0) {
  Element_listy* nowy_element = new Element_listy;
  nowy_element->war=wartosc_do_dodania;
  nowy_element->nastepny=glowa;
  nowy_element->poprzedni=0;
  glowa->poprzedni=nowy_element;
  glowa=nowy_element;
  liczba_elementow_listy++;
  }
  else dodaj_do_pustego(wartosc_do_dodania);
}

void Lista::dodaj_na_koniec(int wartosc_do_dodania) {
  if(liczba_elementow_listy!=0) {
  Element_listy* nowy_element = new Element_listy;
  nowy_element->war=wartosc_do_dodania;
  nowy_element->poprzedni=ogon;
  nowy_element->nastepny=0;
  ogon->nastepny=nowy_element;
  ogon=nowy_element;
  liczba_elementow_listy++;
  }
  else dodaj_do_pustego(wartosc_do_dodania);
}

void Lista::usun(int pozycja_do_usuniecia) {
  if(pozycja_do_usuniecia>=0 && pozycja_do_usuniecia<liczba_elementow_listy) {
    if(liczba_elementow_listy!=0){
      if(pozycja_do_usuniecia==0) {
        usun_z_poczatku();
      }
      else if(pozycja_do_usuniecia==liczba_elementow_listy-1) {
        usun_z_konca();
      }
      else {
      Element_listy* wsk_na_element;
      int iterator_pozycji;
      //usuwanie wzgledem odległości od środka zbioru
        if(pozycja_do_usuniecia<(liczba_elementow_listy/2)) {
          wsk_na_element=glowa;
          iterator_pozycji=0;
          while(iterator_pozycji!=pozycja_do_usuniecia) {
            wsk_na_element=wsk_na_element->nastepny;
            iterator_pozycji++;
          }
        }
        else {
          wsk_na_element=ogon;
          iterator_pozycji=liczba_elementow_listy-1;
          while(iterator_pozycji!=pozycja_do_usuniecia) {
            wsk_na_element=wsk_na_element->poprzedni;
            iterator_pozycji--;
          }
        }
        wsk_na_element->poprzedni->nastepny=wsk_na_element->nastepny;
        wsk_na_element->nastepny->poprzedni=wsk_na_element->poprzedni;
        delete wsk_na_element;
        liczba_elementow_listy--;
    }
    }
    }
}

void Lista::usun_z_poczatku() {
  if(liczba_elementow_listy>=1) {
    Element_listy* wsk=glowa;
    liczba_elementow_listy--;
    if(liczba_elementow_listy==0) {
      delete wsk;
      glowa = 0;
      ogon = 0;
    }
    else {
    glowa->nastepny->poprzedni=0;
    glowa=glowa->nastepny;
    delete wsk;
   }
  }
}

void Lista::usun_z_konca() {
  if(liczba_elementow_listy>=1) {
    liczba_elementow_listy--;
    Element_listy* wsk=ogon;
    if(liczba_elementow_listy==0) {
      ogon=0;
      glowa=0;
      delete wsk;
    }
    else {
    ogon->poprzedni->nastepny=0;
    ogon=ogon->poprzedni;
    delete wsk;
  }
  }
}

int* Lista::wyszukaj(int wartosc_do_wyszukania) {
  Element_listy* wsk=glowa;
  for(int i=0; i<liczba_elementow_listy; i++) {
    if(wsk->war==wartosc_do_wyszukania) {
      return &wsk->war;
    }
    wsk=wsk->nastepny;
  }
  return 0;
}

void Lista::wypisz() {
  Element_listy* wsk=glowa;
  for(int i=0; i<liczba_elementow_listy; i++) {
    cout<<wsk->war<<"  ";
    wsk=wsk->nastepny;
  }
}
