#ifndef LISTA_H
#define LISTA_H


class Element_listy {
public:
  int war;
  Element_listy* nastepny;
  Element_listy* poprzedni;
};

class Lista {
public:
  int liczba_elementow_listy;
  Element_listy* glowa;
  Element_listy* ogon;

public:
  Lista();
  ~Lista();

  void dodaj(int,int);
  void usun(int);
  int* wyszukaj(int);

  void dodaj_do_pustego(int);
  void dodaj_na_poczatek(int);
  void dodaj_na_koniec(int);

  void usun_z_poczatku();
  void usun_z_konca();

  void wypisz();
};

#endif
