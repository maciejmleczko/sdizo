#ifndef KOPIEC_H
#define KOPIEC_H

class Kopiec {
public:
  int* kopiec;
  int liczba_elementow_kopca;
public:
  Kopiec();
  ~Kopiec();

  void dodaj_ost(int);
  void usun_ze_szczytu();
  void napraw_w_gore();
  void napraw_w_dol();
  int* wyszukaj(int);

  void wypisz();
};

#endif
