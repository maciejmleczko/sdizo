#ifndef TABLICA_H
#define TABLICA_H

class Tablica {
public:
  int liczba_elementow_tablicy;
  int* tablica;
public:
  Tablica();
  ~Tablica();

  void dodaj(int,int);
  void usun(int);
  int* wyszukaj(int);

  void wypisz();
};

#endif
