#ifndef MENU_H
#define MENU_H

#include <iostream>
#include "test.h"


using namespace std;

class Menu {
  private:
    char wybor;
    Test test;
  public:
     Menu();
     ~Menu();
     void uruchom_program();
     void wyswietl_menu_testow();
  private:
    void wczytaj_dane_testowe();
    void wyswietl_menu_glowne();
    void wyswietl_dostepne_struktury();
    void wyswietl_podmenu_wczytywania();
    void uruchom_test();
};

#endif
