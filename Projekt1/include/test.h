#ifndef TEST_H
#define TEST_H

#include "lista.h"
#include "tablica.h"
#include "kopiec.h"

class Test {
public:
  int rozmiar_zestawu;
  int* zestaw;
  Lista* lista;
  Tablica* tablica;
  Kopiec* kopiec;

public:
  Test();
  ~Test();
  void generuj_nowy_zestaw();

  void generuj_lista();
  void generuj_tablica();
  void generuj_kopiec();

  void wyczysc_aktywna_strukture();


  double dodawanie_tablica();

  double dodawanie_do_srodka_zbioru_lista();
  double dodawanie_na_poczatek_lista();
  double dodawanie_na_koniec_lista();

  double dodawanie_kopiec();


  double usuwanie_tablica();

  double usuwanie_ze_srodka_zbioru_lista();
  double usuwanie_z_poczatku();
  double usuwanie_z_konca();

  double usuwanie_wierzcholka_kopca();


  double wyszukiwanie_lista();
  double wyszukiwanie_tablica();
  double wyszukiwanie_kopiec();

};

#endif
